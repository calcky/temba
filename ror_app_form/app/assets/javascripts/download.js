// behavior of browser when the form is already sent to the server, the server
// and the client is waiting to obtain it
//   thanks darkdrgn2k for your help and support!
//   src https://stackoverflow.com/questions/21789425/simple-xhr-long-polling-without-jquery

var xhr = new XMLHttpRequest()
xhr.responseType = 'text' //or 'text', 'json', ect. there are other types.
xhr.timeout = 60000 //milliseconds until timeout fires. (1 minute)

// polling logic: it tries to get the file regularly
xhr.onload = function() {
  content = xhr.responseText
  if(content != "not available") {
    //console.log('DEBUG: file downloaded successfully')
    document.getElementsByClassName('loader')[0].style.display = "none"
    document.location = content
  }
  // retry after 4 seconds
  else {
    //console.log('DEBUG: tried, but file is not available yet')
    setTimeout("get_file()", 4000)
  }
}

xhr.ontimeout = function(){
  //if you get this you probably should try to make the connection again.
  //the browser should've killed the connection.
}

// displays a loader animation and starts polling (xhr function) with the
// appropriate data it got from the notice section
function get_file() {
  // visibility -> src https://stackoverflow.com/questions/42389937/trying-to-toggle-visibility-of-classes-with-buttons-using-javascript-absolute
  document.getElementsByClassName('loader')[0].style.display = "block"
  var notice = document.getElementById('notice').textContent
  var file_name = notice.split(':')[1].trim() + '.zip'
  var download_req = "/download?file=" + file_name
  xhr.open('GET', download_req, true)
  xhr.send()
}

// when the browser finds a specific text match in notice section, then it
// starts polling for firmware download
//   src https://stackoverflow.com/questions/799981/document-ready-equivalent-without-jquery
document.addEventListener('DOMContentLoaded', function() {
   // TODO change test id (notice or what)
   var notice = document.getElementById('notice').textContent
   if ( notice.match('Petition received to build node') ) {
     get_file()
   }
})

