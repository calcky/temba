# custom functions for imagebuider.sh

# load functions:
#   git_apply_patch
#   copy_patch
. imagebuilder-customfuns-lib.sh

install_custom_packages() {
  if [ $dtun_package = 'y' ]; then
    dtun_git='src-git dtun https://gitlab.com/guifi-exo/dtun.git;master'
    ! grep -q "$dtun_git" feeds.conf &> /dev/null && echo "$dtun_git     # dtun: a custom package we use" >> feeds.conf
    dtun_config=$(cat << _EOF || :
# custom package to use gre with dynamic IPs
CONFIG_PACKAGE_dtun=y
_EOF
    )
  fi
}

# the principle is that if patch is not applied, then it is removed
install_custom_patches() {

  # latest stable bmx6 version
  local patch_var="$latest_stable_bmx6_patch"
  local target_path="feeds/routing/bmx6/"
  local patch_file="Makefile"
  local patch_path="../patches/bmx6"
  copy_patch "$patch_var" \
             "$target_path" \
             "$patch_file" \
             "$patch_path"

  # compliance test patch
  if [ $compliance_test_patch = 'y' ]; then
    cp -v ../patches/999-compliance-test.patch package/firmware/wireless-regdb/patches/
    # src https://dev.archive.openwrt.org/ticket/6923
    compliance_test_config='CONFIG_ATH_USER_REGD=y'
    # about 'CONFIG_PACKAGE_ATH_DFS=y' -> src https://lists.berlin.freifunk.net/pipermail/berlin/2014-July/025144.html
  else
    rm -fv package/firmware/wireless-regdb/patches/999-compliance-test.patch
  fi

  # netifd egress ingress patch
  local patch_var="$netifd_egress_ingress_patch"
  local target_path="package/network/config/netifd/patches/"
  local patch_file="999-netifd-egress-ingress.patch"
  copy_patch "$patch_var" \
             "$target_path" \
             "$patch_file"

  # ubnt version patch
  local patch_var="$ath79_update_ubnt_version_patch"
  local target_path="target/linux/ath79/image"
  local target_file="generic-ubnt.mk"
  local patch_file="${target_file}_ubntversion.patch"
  local patch_path="../patches"
  git_apply_patch "$patch_var" \
              "$patch_file" \
              "$patch_path"

  # change from ath10k-ct to ath10k
  local patch_var="$ath79_update_ath10k_patch"
  local target_path="target/linux/ath79/image"
  local target_file="generic-ubnt.mk"
  local patch_file="${target_file}_ath10k.patch"
  local patch_path="../patches"
  git_apply_patch "$patch_var" \
              "$patch_file" \
              "$patch_path"

  # phicomm wifi support
  local patch_var="$phicomm_k2p_patch"
  local target_path="target/linux/ramips/image"
  local target_file="mt7621.mk"
  local patch_file="${target_file}_phicomm_k2p.patch"
  local patch_path="../patches"
  git_apply_patch "$patch_var" \
              "$patch_file" \
              "$patch_path"

  local patch_var="$xiaomi_mi_4a_patch"
  local target_path="target/linux/ramips/image"
  local target_file="mt7621.mk"
  local patch_file="${target_file}_xiaomi_mi_4a.patch"
  local patch_path="../patches"
  git_apply_patch "$patch_var" \
              "$patch_file" \
              "$patch_path"

  local patch_var="$fix_bmx6info_patch"
  local target_path="feeds/routing/bmx6/"
  local patch_file="999-fix_bmx6info.patch"
  local patch_path="../patches"
  copy_patch "$patch_var" \
             "$target_path" \
             "$patch_file" \
             "$patch_path"

  # TODO: add to the docs
  # ## notes on dirty patch example (based on the pespin patch and inspired by marec's workflow)
  # # tp = target path
  # tp='package/network/config/netifd/patches/vlan-priority-pespin.patch'
  # # pu = patch url (this method works for gitlab and github)
  # # get new SHA (cherrypicking case) -> src https://github.com/pespin/netifd/commits/pespin/master
  # pu='https://github.com/pespin/netifd/commit/1b2d49a2d9e5a7c6547738ec89675855feee981a.patch'
  # mkdir -p $(dirname "$tp")
  # #cp -v ../patches/vlan-priority-pespin.patch package/network/config/netifd/patches/
  # wget "$pu" -O "$tp" -nc || :
  # ##rm -v package/network/config/netifd/patches/vlan-priority-pespin.patch
  # # to do a fast compilation while developing:
  # #   make package/netifd/{clean,compile} V=s
}
